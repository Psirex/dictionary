import { List, Map } from 'immutable';
import translate from '../translateApi';
import { combineReducers } from 'redux';

const initState = {
  text: '',
  isFetched: false,
  translations: [],
  error: null
};

export default function translationReducer(state=initState, action) {
  console.log(state);
  console.log(action);
  switch(action.type) {
    case 'TRANSLATE_REQUEST':
      return {
        text: state.text,
        isFetched: true,
        translations: [...state.translations],
        error: null
      }
    break;
    case 'TRANSLATE_SUCCESS':
      return {
        text: state.text,
        isFetched: true,
        translations: [...action.payload.translations],
        error: null
      }
    break;
    case 'TRANSLATE_FAILED':
      return {
        text: state.text,
        isFetched: false,
        translations: [...state.translations],
        error: action.error
      }
    break;
    default:
      return state;
  }
}

// export default combineReducers({
//   translationReducer
// });
