import {Record, List} from 'immutable';
const TranslationRecod = Record({
  word: '',
  translations: List()
});

const TrItem = Record({
    text: '',
    exs: List(),
    syns: List()
});

const ExItem = Record({
  text: '',
  tr: ''
})

const SynItem = Record({
  text: ''
});

const DefItem = Record({
  pos: '',
  text: '',
  ts: '',
  trs: List()
});

const addr = 'https://translate.yandex.net/api/v1.5/tr.json/translate?';
const key = 'trnsl.1.1.20160919T174645Z.9f58551bb79d538c.17bd1f1afe84d9a84c04be283494b56bff3fbbfe';
const keyDict = 'dict.1.1.20161007T221025Z.272d665cb72038e8.37ce921d33801586cb9ba5577cec0c5d6533ad79';
const addrDict = 'https://dictionary.yandex.net/api/v1/dicservice.json/lookup?'
function getAddr(){
  return addr + 'lang=en-ru&key=' + key;
}

function getDictAddr(word, fromLang, toLang){
  return addrDict + 'key='+ keyDict + `&lang=${fromLang.toLowerCase()}-${toLang.toLowerCase()}&text=` + word;
}

function processExItem({text, tr}) {
  return new ExItem({
    text,
    tr: tr[0].text
  })
}

function processSynItem({text, ...rest}) {
  return new SynItem({text});
}

function processTrItem({ex, mean, syn, text}) {
  // logFunctionName(processTrItem);
  // console.log(ex);
  // console.log(mean);
  // console.log(syn);
  // console.log(text);
  let exs = List();
  let syns = List();
  if (ex !== undefined) {
    ex.forEach((item) => {
      exs = exs.push(processExItem(item))
    });
    // exs = ex.map(processExItem);
  }
  if (syn !== undefined) {
    syn.forEach((item)=>{
      syns = syns.push(processSynItem(item));
    });
    // syns = syn.map(processSynItem);
  }
  return new TrItem({
    text,
    exs,
    syns
  })
  // return {
  //   text,
  //   exs,
  //   syns
  // };
}

function logFunctionName(func) {
  console.log(func.name);
}


function processDefItem({pos, text, tr, ts}) {
  // logFunctionName(processDefItem);
  let trs = List();
  tr.forEach((item) => {
    trs = trs.push(processTrItem(item));
  });
  // let trs = tr.map(processTrItem);
  return new DefItem({
    pos,
    text,
    ts,
    trs
  })
  // return {
  //   pos,
  //   text,
  //   ts,
  //   trs
  // };
}

export async function translate(text, fromLang='en', toLang='ru') {
  try {
    let response = await fetch(getDictAddr(text, fromLang, toLang),
    {
      method: 'POST'
    });
    responseJSON = await response.json();
    // console.log(responseJSON);
    let translationsList = List();
    responseJSON.def.forEach((item) => {
      translationsList = translationsList.push(processDefItem(item));
    });
    let translationRes = new TranslationRecod({
      word: text,
      translations: translationsList
    });
    let fullRes = responseJSON.def.map(processDefItem);
    return {text, translations: translationsList};
  } catch (err) {
    console.log(err);
    return {};
  }
}

async function testRequest() {
  try {
    let response = await fetch(getAddr()+'&text=column', {
      method: 'POST',
      // body: JSON.stringify({text:'Hello World'})
    });
    let responseJson = await response.json();
    console.log(responseJson.text[0]);
    response = await fetch(getDictAddr('column name'), {method: 'POST'});
    responseJson = await response.json();
    console.log(responseJson);
  }catch(err){
    console.log(err);
    console.log(getAddr());
  }
}
