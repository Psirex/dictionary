import React, {Component} from 'react';
import {View,
  Text,
  StyleSheet} from 'react-native';

class Chart extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View style={styles.chart}>
        <View style={styles.circle}>
          <Text style={styles.text}>1926р</Text>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  chart: {
    width: 372,
    height: 200,
    backgroundColor: 'darkseagreen',
  },
  text: {
    color: 'white',
    fontSize: 55,
    lineHeight: 190,
    textAlign: 'center',
    borderRadius: 1,
    borderColor: 'red'
  },
  circle: {
    width : 190,
    height : 190,
    marginTop : 5,
    marginLeft : 5,
    borderRadius: 95,
    borderWidth: 4,
    borderColor: 'white',
    backgroundColor: 'transparent'
  }
});

module.exports = Chart;
