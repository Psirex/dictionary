import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TextInput,
    Image
} from 'react-native';

class WeatherProject extends Component {
    constructor(props) {
        super(props);
        this.state = {zip:''};
    }
    _handleTextChange(event) {
        console.log(event.nativeEvent.text);
        this.setState({
            zip: event.nativeEvent.text
        });
    }
    render(){
        return (
            <View style={styles.container}>
                <Text style={styles.welcome}>
                    Your input {this.state.zip}
                </Text>
                <TextInput
                    style={styles.input}
                    onSubmitEditing={this._handleTextChange}/>
            </View>
            );
    }
}

var styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#f5fcff',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10
    },
    input: {
        fontSize: 20,
        borderWidth: 2,
        height: 40
    }
});
module.exports = WeatherProject;