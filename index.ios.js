import React, { Component } from 'react';
import {Map, List} from 'immutable';
import { Text,
  View,
  TouchableHighlight,
  TouchableOpacity,
  TextInput,
  TouchableWithoutFeedback,
  Keyboard,
  TabBarIOS,
  Picker} from 'react-native';
  const dismissKeyboard = require('dismissKeyboard');
import {AppRegistry, StyleSheet, LayoutAnimation} from 'react-native';
const translate = require('./translateApi');
// import TranslateScreen from './components/TranslateScreen';
import Reducer from './reducers/reducers';

import { Provider } from 'react-redux';
import { createStore, applyMiddleware, combineReducers, compose } from 'redux';
import thunkMiddleware from 'redux-thunk';
import createLogger from 'redux-logger';
import reducer from './app/reducers';
import AppContainer from './app/containers/AppContainer';
import TranslateAppContainer from './app/containers/AppContainerTranslate';

const loggerMiddleware = createLogger({
  predicate: (getState, action) => __DEV__
});

function configureStore(initialState) {
  const enhancer = compose(
    applyMiddleware(thunkMiddleware,
      loggerMiddleware)
  );
  return createStore(reducer, initialState, enhancer);
}

const store = configureStore({});

class WeatherProject extends Component {
  constructor(props) {
    super(props);
    this.state={selectedTab: 'translate'}
  }
  componentWillMount() {
      translate('column');
      LayoutAnimation.spring();
  }

  render() {
    return (
      <TabBarIOS>
        <TabBarIOS.Item
          title="Перевод"
          selected={this.state.selectedTab === 'translate'}
          onPress= {() => {
            this.setState({selectedTab: 'translate'})
          }}
        >
          <TouchableWithoutFeedback onPress={this.cancelTouch.bind(this)}>
          <TranslateScreen/>
          </TouchableWithoutFeedback>
        </TabBarIOS.Item>
        <TabBarIOS.Item
          title="Тест"
          selected={this.state.selectedTab === 'test'}
          onPress= {() => {
            this.setState({selectedTab: 'test'})
          }}
        >
          <View style={{backgroundColor: 'red', flex:1}}>
          </View>
        </TabBarIOS.Item>
        <TabBarIOS.Item
          title="Словарь">
          <View style={{backgroundColor: 'red', flex:1}}>
          </View>
        </TabBarIOS.Item>
      </TabBarIOS>
    );
  }
  buttonClicked() {
    console.log('button clicked');
    LayoutAnimation.spring();
    this.setState({style: {top: -100}, text: this.state.text});
  };
  animationBack() {
    LayoutAnimation.spring();
    this.setState({style: {top: 0}, text: this.state.text});
  }
  cancelTouch() {
    console.log('dismiss');
    this.animationBack();
    dismissKeyboard();
  }
  longButtonClicked() {
    console.log('long button clicked');
  }

}

var styles = StyleSheet.create({
  mainWindow: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    alignSelf: 'stretch',
    justifyContent: 'flex-start',
    paddingTop: 80
  },
  button: {
    width: 100,
    height: 100,
    backgroundColor: 'skyblue'
  },
  textView: {
    borderWidth:1,
    borderColor: 'gray',
    height: 40,
    marginTop: 10,
    width: 340
  }
});

const App = () => (
  <Provider store={store}>
    <TranslateAppContainer />
  </Provider>
);

AppRegistry.registerComponent('AwesomeProject', () => App );
