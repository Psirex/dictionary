import {AsyncStorage} from 'react-native'

function isCharInString (str, from, to) {
  // console.log(`${str} ${from} ${to}`)
  let result = true
  let lowerCaseStr = str.toLowerCase()
  for (let i = 0; i < lowerCaseStr.length; ++i) {
    // console.log()
    result &= (lowerCaseStr.charCodeAt(i) >= from && lowerCaseStr.charCodeAt(i) <= to)
  }
  console.log(`${str} ${from} ${to} ${result}`)
  return result
}

export function isRussianWorld (str) {
  return isCharInString(str, 1072, 1105)
}

export function isEnglishWorld (str) {
  return isCharInString(str, 97, 122)
}

function getRandIntsInRange (count, from, to) {
  let res = []
  for (let i = 0; i < count; ++i) {
    let ind = getRandomInt(from, to)
    while (res.includes(ind)) {
      let shift = getRandomInt(from, to)
      ind = (ind + shift) % to
      if (ind < from) {
        ind += from
      }
    }
    res.push(ind)
  }
  console.log(res)
  return res
}

function getRandomInt (min, max) {
  return Math.floor(Math.random() * (max - min)) + min
}

function getRandPosAndTrInd (posCount, trsCount) {
  return {
    posInd: getRandomInt(0, posCount),
    trsInd: getRandomInt(0, trsCount)
  }
}

function getRandAnswer (wordsTranslations, correctIndex) {
  let res = {
    word: wordsTranslations[correctIndex].word,
    cases: [],
    examples: [],
    correctIndex
  }
  wordsTranslations.forEach((item, index) => {
    let posInd = getRandomInt(0, item.translations.length)
    let trsInd = getRandomInt(0, item.translations[posInd].trs.length)
    res.cases.push(item.translations[posInd].trs[trsInd].text)
    if (index === correctIndex) {
      res.examples = item.translations[posInd].trs[trsInd].exs
    }
  })
  return res
}

function formatWord (word) {
  return word[0].toUpperCase() + word.slice(1, word.length).toLowerCase()
}

export default class DictionaryStorage {
  static saveWord (word, translations) {
    console.log('dictionary save')
    let formedWord = word[0].toUpperCase() + word.slice(1, word.length).toLowerCase()
    return AsyncStorage.setItem(formedWord, JSON.stringify(translations))
  }
  static getWordTranslations (word) {
    return AsyncStorage.getItem(word)
  }
  // static getAllWords(){
  //   return AsyncStorage.getAllKeys()
  // }

  static async getTest (lang) {
    const COUNT_TESTS = 4
    let res = {
      word: '',
      examples: [],
      correctIndex: -1,
      cases: []
    }
    try {
      const wordKeys = await DictionaryStorage.getAllWords(lang)
      const inds = getRandIntsInRange(COUNT_TESTS, 0, wordKeys.length)
      let promises = inds.map((item) => {
        return {
          word: wordKeys[item],
          promise: DictionaryStorage.getWordTranslations(wordKeys[item])
        }
      })
      let wordsTranslations = []
      for (let item of promises) {
        let translations = await item.promise

        wordsTranslations.push({
          word: item.word,
          translations: JSON.parse(translations)
        })
      }
      let correctAnswerIndex = getRandomInt(0, 4)

      let r = getRandAnswer(wordsTranslations, correctAnswerIndex)
      console.log(r)
      return r
    } catch (err) {
      console.log(err)
    }
  }

  static async presentInDictionary (word) {
    console.log(formatWord(word))
    const words = await DictionaryStorage.getAllWords()
    return words.includes(formatWord(word))
  }

  static async getAllWords (lang) {
    try {
      const keys = await AsyncStorage.getAllKeys()
      let res = []
      console.log(lang)
      console.log(keys)
      if (keys !== null) {
        switch (lang) {
          case 'RUS':
            res = keys.filter(isRussianWorld)
            break
          case 'ENG':
            res = keys.filter(isEnglishWorld)
            break
          default:
            res = keys
            break
        }
        return res
      }
    } catch (err) {
      console.log(err)
    }
  }
}
