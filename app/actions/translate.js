import * as types from './types'
import * as apiTranslate from '../../translateApi'
import DictionaryStorage from '../storage/DictionaryStorage'
export function translate (text, fromLang, toLang) {
  return (dispatch, getState) => {
    dispatch(startTranslate())
    let translatePromise = apiTranslate.translate(text, fromLang, toLang)
    let presentedPromise = DictionaryStorage.presentInDictionary(text)
    return Promise.all([translatePromise, presentedPromise]).then(values => {
      console.log(values)
      dispatch(setTranslate(values[0], values[1]))
    }).catch((err) => {
      console.log(err)
    })
    // return apiTranslate.translate(text, fromLang, toLang)
    //   .then((answer) => {
    //     dispatch(setTranslate(answer))
    //   })
    //   .catch((err) => { console.log(err) })
  }
}

export function setTranslate ({ text, translations }, isPresented) {
  return {
    type: types.SET_TRANSLATE,
    word: text,
    isPresented,
    translations
  }
}

export function changeWord(word) {
  return {
    type: types.CHANGE_WORD,
    word: word
  }
}

export function swapLanguages() {
  return {
    type: types.SWAP_LANGUAGES
  }
}

export function swapLanguagesAndTranslate(text) {
  return (dispatch, getState) => {
    dispatch(swapLanguages());
    return dispatch(translate(text,
      getState().translationScreen.from.slice(0,2).toLowerCase(),
      getState().translationScreen.to.slice(0,2).toLowerCase()));
  }
}

export function startTranslate() {
  return {
    type: types.START_TRANSLATE
  }
}

export function finishTranslate() {
  return {
    type: types.FINISH_TRANSLATE
  }
}
