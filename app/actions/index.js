import * as TranslateActions from './translate';
import * as TabBarActions from './tabBar';
import * as TestsActions from './tests';
import * as DictionaryActions from './dictionary';
export const ActionCreators = Object.assign({},
  TranslateActions, TabBarActions, TestsActions,
  DictionaryActions);
