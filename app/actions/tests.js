import * as types from './types';
import DictionaryStorage from '../storage/DictionaryStorage'

export function testItemPressed (index) {
  return {
    type: types.TEST_ITEM_PRESSED,
    index: index
  }
}

export function showAnswer () {
  return {
    type: types.SHOW_ANSWER
  }
}

export function loadTest (lang, nextTest) {
  return (dispatch, getState) => {
    nextTest = nextTest || DictionaryStorage.getTest(lang)
    let currentTest = DictionaryStorage.getTest(lang)
    Promise.all([nextTest, currentTest]).then(values => {
      console.log(values)
      dispatch(setTest(values[0], values[1]))
    })
    // return DictionaryStorage.getTest(lang).then((answer) => {
    //   dispatch(setTest(answer))
    // })
  }
}

export function setTest (first, second) {
  console.log(first)
  console.log(second)
  return {
    type: types.SET_TEST,
    first,
    second
  }
}
