import * as types from './types';

export function tabBarItemPressed (title) {
  return {
    type: types.TAB_BAR_PRESSED,
    title: title
  }
}
