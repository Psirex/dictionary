import * as types from './types'
import DictionaryStorage from '../storage/DictionaryStorage'
import immutable from 'immutable'

export function loadWords (lang) {
  return (dispatch, getState) => {
    return DictionaryStorage.getAllWords(lang).then(
      (words) => {
        console.log(words)
        dispatch(setWords(immutable.fromJS(words)))
      }
    ).catch((err) => { console.log(err) })
  }
}

export function setWords (words) {
  return {
    type: types.SET_WORDS,
    words: words
  }
}

export function swapDictionaryLanguages () {
  return {
    type: types.SWAP_DICTIONARY_LANGUAGES
  }
}

export function addWord (word) {
  return {
    type: types.ADD_WORD,
    word: word
  }
}

export function wordWasAdded () {
  return {
    type: types.WORD_WAS_ADDED
  }
}

export function saveWord (word, translations) {
  return (dispatch, getState) => {
    return DictionaryStorage.saveWord(word, translations).then(
      (res) => {
        console.log(word)
        console.log(res)
        dispatch(addWord(word))
        dispatch(wordWasAdded())
      }
    ).catch((err) => { console.log(err) })
  }
}

export function toggleTranslation (index) {
  console.log(index)
  return {
    type: types.TOGGLE_TRANSLATION,
    index: index
  }
}

export function hideTranslation (word) {
  return {
    type: types.HIDE_TRANSLATION,
    word
  }
}

export function loadWord (word) {
  return (dispatch, getState) => {
    return DictionaryStorage.getWordTranslations(word).then((res) => {
      const resObj = JSON.parse(res)
      console.log(immutable.fromJS(resObj))
      dispatch(addTranslationToDictionary(word, immutable.fromJS(resObj)))
    })
  }
}

export function addTranslationToDictionary (word, translations) {
  return {
    type: types.ADD_TRANSLATIONS_TO_DICTIONARY,
    word,
    translations
  }
}

// export function saveWord(word, translations) {
//   return (dispatch, getState) => {
//     // return DictionaryStorage.saveWord(word, translations).then(
//     //   (res) => {
//     //     console.log(word)
//         dispatch(addWord('word'));
//     //   }
//     // ).catch((err) => {console.log(err)})
//   }
// }

// export function swapDictionaryLanguagesAndTranslate() {
//   return (dispatch, getState) => {
//     dispatch(swapDictionaryLanguages());
//     return dispatch(loadWords(getState().dictionaryScreen.from));
//   }
// }
