import { StyleSheet } from 'react-native';

export const style = {
  shadow: {
    shadowOpacity: 1,
    shadowColor: 'rgb(221,221,221)',
    shadowRadius: 3,
    shadowOffset: {
      widht: 0,
      height: 0
    }
  }



}

const SHADOW = {
  shadowOpacity: 1,
  shadowColor: 'rgb(221,221,221)',
  shadowRadius: 3,
  shadowOffset: {
    widht: 0,
    height: 0
  }
}
export function createButtonStyle(width, height, otherProps) {

  let buttonBase = {
      backgroundColor: 'white',
      borderRadius : 5,
      borderColor: 'rgb(201, 201, 201)',
      borderWidth : 1,
      justifyContent: 'center'
    }

  return StyleSheet.flatten([buttonBase, style.shadow, { width, height }, otherProps])
  // return combineStyles({ width, height }, props, SHADOW);
}

export function shadow(props) {
  return
}

export function combineStyles() {
  return Object.assign({}, ...Array.from(arguments).slice(0));
}
