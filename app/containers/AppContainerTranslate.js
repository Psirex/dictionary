import React, {Component} from 'react';
import {
  View,
  Text,
  TabBarIOS
} from 'react-native';
import { connect } from 'react-redux';
import { ActionCreators } from '../actions';
import { bindActionCreators } from 'redux';
import TranslateScreen from '../components/TranslateScreen';
import TestsScreen from '../components/TestsScreen';
import DictionaryScreen from '../components/DictionaryScreen';

class TranslateAppContainer extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <TabBarIOS>
        <TabBarIOS.Item
          title="Перевод"
          selected={this.props.tabBar.selected === "Перевод"}
          icon={require('./text.png')}
          onPress={
            () => {this.props.tabBarItemPressed("Перевод")}
          }
          >
          <TranslateScreen
            changeWord={this.props.changeWord}
            word={this.props.word}
            translations={this.props.translations}
            translate={this.props.translate} />
        </TabBarIOS.Item>
        <TabBarIOS.Item
          title="Тесты"
          selected={this.props.tabBar.selected === "Тесты"}
          icon={require('./trophy.png')}
          onPress={
            () => {this.props.tabBarItemPressed("Тесты")}
          }
          >
          <TestsScreen />
        </TabBarIOS.Item>
        <TabBarIOS.Item
          title="Словарь"
          selected={this.props.tabBar.selected === "Словарь"}
          icon={require('./text-list.png')}
          onPress={
            () => {this.props.tabBarItemPressed("Словарь")}
          }
          >
          <DictionaryScreen />
        </TabBarIOS.Item>
      </TabBarIOS>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
}

export default connect((state) => {
  return {
    tabBar: state.tabBar
  }
}, mapDispatchToProps)(TranslateAppContainer);
