import { combineReducers } from 'redux';
import * as translateReducers from './translate';
import * as tabBarReducers from './tabBar';
import * as testsScreenReducers from './tests';
import * as dictionaryReducers from './dictionary';
export default combineReducers(Object.assign({},
  translateReducers,
  tabBarReducers,
  testsScreenReducers,
  dictionaryReducers));
