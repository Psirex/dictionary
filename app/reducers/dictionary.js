import * as types from '../actions/types';
import createReducer from '../lib/createReducer';
import { Record, Map, List } from 'immutable';
// const ShowedItems = Record({})

class ShowedItemRecord extends Record({ word: '', translations: List() }) {
  isEmpty() {
    return this.translations.size === 0;
  }
}

const DictionaryRecord = new Record({
  words: List(),
  showedItem: new ShowedItemRecord(),
  from: 'ENG',
  to: 'RUS'
});

export const dictionaryScreen = createReducer(
  new DictionaryRecord()
,{
  [types.SET_WORDS](state, action) {
    return state.set('words', action.words);
  },
  [types.SWAP_DICTIONARY_LANGUAGES](state, action) {
    const from = state.get('from');
    return state.set('from', state.get('to')).set('to', from);
  },
  [types.ADD_WORD](state, action) {
    console.log('addWord');
    if (!state.get('words').includes(action.word)) {
      return state.set('words', state.get('words').push(action.word));
    }
    return state;
  },
  [types.TOGGLE_TRANSLATION](state, action) {
    let newWords = [...state.words]
    newWords[action.index].hidden = !newWords[action.index].hidden;
    return {
      from: state.from,
      to: state.to,
      words: newWords,
      showedItems: {...state.showedItems}
    }
  },
  [types.HIDE_TRANSLATION](state, action) {
    return state.set('showedItem', new ShowedItemRecord());
  },

  [types.ADD_TRANSLATIONS_TO_DICTIONARY](state, action) {
    const newShowedItem = state.get('showedItem').merge({
      word: action.word,
      translations: action.translations
    });
    return state.set('showedItem', newShowedItem);
  }
})
