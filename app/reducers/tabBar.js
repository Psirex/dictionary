import createReducer from '../lib/createReducer';
import * as types from '../actions/types';

export const tabBar = createReducer({
  selected: 'Словарь'
}, {
  [types.TAB_BAR_PRESSED](state, action) {
    console.log('TAB_BAR_PRESSED');
    return {
      selected: action.title
    }
  }
});
