import createReducer from '../lib/createReducer';
import * as types from '../actions/types';
import {Record, List} from 'immutable';
const TranslationRecod = new Record({
  word: '',
  wordWasTranslated: '',
  translations: List(),
  isLoading: false,
  from: 'ENG',
  to: 'RUS',
  isPresented: false
})
export const translationScreen = createReducer(
  new TranslationRecod()
, {
  [types.CHANGE_WORD] (state, action) {
    console.log(state)
    return state.set('word', action.word)
  },
  [types.SET_TRANSLATE] (state, action) {
    console.log(state.toJS())
    return state.set('wordWasTranslated', action.word)
      .set('translations', action.translations)
      .set('isLoading', false)
      .set('isPresented', action.isPresented)
  },
  [types.SWAP_LANGUAGES] (state, action) {
    let from = state.get('from')
    return state.set('from', state.get('to'))
      .set('to', from)
  },
  [types.START_TRANSLATE] (state, action) {
    return state.set('isLoading', true)
  },
  [types.FINISH_TRANSLATE] (state, action) {
    return state.set('isLoading', false)
  },
  [types.WORD_WAS_ADDED] (state, action) {
    return state.set('isPresented', true)
  }
})
