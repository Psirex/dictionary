import createReducer from '../lib/createReducer'
import * as types from '../actions/types'

export const testsScreen = createReducer({
  first: {
    word: 'Example',
    correctIndex: 0,
    // selectedItem: -1,
    // showAnswer: false,
    examples: [],
    cases: [
      'Пример',
      'Привет',
      'Машина',
      'Кактус'
    ]
  },
  second: {
    word: 'Example',
    // correctIndex: 0,
    // selectedItem: -1,
    showAnswer: false,
    examples: [],
    cases: [
      'Пример',
      'Привет',
      'Машина',
      'Кактус'
    ]
  }
}, {
  // [types.TEST_ITEM_PRESSED] (state, action) {
  //   console.log(state)
  //   console.log(action)
  //   return {
  //     first: {
  //       word: state.first.word,
  //       correctIndex: state.first.correctIndex,
  //       selectedItem: action.index,
  //       cases: [...state.first.cases],
  //       examples: [...state.first.examples],
  //       showAnswer: state.first.showAnswer
  //     },
  //     second: {
  //       word: state.second.word,
  //       correctIndex: state.second.correctIndex,
  //       selectedItem: -1,
  //       cases: state.second.cases,
  //       examples: state.second.examples,
  //       showAnswer: false
  //     }
  //   }
  // },
  // [types.SHOW_ANSWER] (state, action) {
  //   return {
  //     first: {
  //       word: state.first.word,
  //       correctIndex: state.first.correctIndex,
  //       selectedItem: state.first.selectedItem,
  //       cases: [...state.first.cases],
  //       examples: [...state.first.examples],
  //       showAnswer: true
  //     },
  //     second: {
  //       word: state.second.word,
  //       correctIndex: state.second.correctIndex,
  //       selectedItem: -1,
  //       cases: state.second.cases,
  //       examples: state.second.examples,
  //       showAnswer: false
  //     }
  //   }
  // },
  [types.SET_TEST] (state, action) {
    console.log(action)
    return {
      first: {
        word: action.first.word,
        correctIndex: action.first.correctIndex,
        selectedItem: -1,
        cases: action.first.cases,
        examples: action.first.examples,
        showAnswer: false
      },
      second: {
        word: action.second.word,
        correctIndex: action.second.correctIndex,
        selectedItem: -1,
        cases: action.second.cases,
        examples: action.second.examples,
        showAnswer: false
      }
    }
  }

})
