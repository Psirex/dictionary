module.exports =  function getSectionData(translations){
  console.log(translations);
  let res = translations.reduce((currentState, item) => {
    //Создается ключ для части речи в объекте
    currentState[item.pos] = [];
    //Обрабатываем каждый элемент перевода
    item.trs.forEach((tr) => {
      //Собрали все синонимы в строку
      let syns = tr.text;
      if (tr.syns !== undefined) {
        syns = tr.syns.reduce((currentState, item) => {
          return currentState + ', ' + item.text
        }, tr.text);
      }
      currentState[item.pos].push({
        text: syns,
        exs: tr.exs
      });
    });
    return currentState;
  },{});
  return res;
}
