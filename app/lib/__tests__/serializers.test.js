"use strict"
const getSectionData = require('../serializers');
test('test for TranslationsList serializer', () => {
  expect(getSectionData(fakeData[0][0])).toEqual(fakeData[0][1]);
});

var fakeData = [
  [
    [
      {
        pos: 'noun',
        text: 'test',
        trs: [],
        ts: 'test'
      },
      {
        pos: 'adjective',
        text: 'test',
        trs: [],
        ts: 'test'
      }
    ],
  {
    'noun' : [],
    'adjective' : []
  }
  ]
]
