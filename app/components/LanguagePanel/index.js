import React, {Component} from 'react'
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity
} from 'react-native'

class LanguagePanel extends Component {
  constructor (props) {
    super (props);
  }

  render () {
    return (
      <View style={styles.wrapper}>
        <Text style={styles.language}>{this.props.from.toUpperCase()}</Text>
        <TouchableOpacity onPress={this.props.swapLanguages}>
          <Image style={styles.swapLanguages} source={require('./swap.png')}/>
        </TouchableOpacity>
        <Text style={[styles.language, styles.toLanguage]}>{this.props.to.toUpperCase()}</Text>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  wrapper: {
    // backgroundColor: 'gray',
    flexDirection: 'row',
    // width: 210,
    height: Style.getAdaptHeight(30),
    marginRight: Style.getAdaptWidth(4)
  },
  language: {
    // backgroundColor: 'green',
    width: Style.getAdaptWidth(50),
    textAlign: 'center',
    lineHeight: Style.getAdaptHeight(30),
    fontSize: 20,
  },
  toLanguage: {
    marginLeft: Style.getAdaptWidth(10)
  },
  swapLanguages: {
    // backgroundColor: 'blue',
    width: Style.getAdaptWidth(40),
    height: Style.getAdaptHeight(30),
    marginLeft: Style.getAdaptWidth(10)
  }
})

module.exports = LanguagePanel;
