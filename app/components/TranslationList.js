import React, {Component} from 'react';
import {
  View,
  Text,
  ListView,
  StyleSheet,
  ScrollView,
  PanResponder
} from 'react-native';
const immutable = require('immutable');
class TranslationList extends Component {

  componentWillReceiveProps (nextProps) {
    console.log('componentWillReciveProps')
    this.setState({
      dataSource: this.state.dataSource
        .cloneWithRowsAndSections(nextProps.translations)
    })
    // console.log(this.getSectionData(nextProps.translations));
  }

  componentDidMount () {
    if (this.props.onDidMount) {
      this.props.onDidMount()
    }
  }

  componentWillMount () {
    this._panResponder = PanResponder.create({
      onMoveShouldSetPanResponderCapture: () => false,
      onStartShouldSetPanResponderCapture: () => false,
      onStartShouldSetPanResponder: () => true,
      onShouldBlockNativeResponder: (event, gestureState) => true,
      onPanResponderRelease: (e, {vx, vy}) => {
        console.log('heyyyyy')
      }
    })
    this._panResponderInner = PanResponder.create({
      onMoveShouldSetPanResponderCapture: () => false,
      onStartShouldSetPanResponderCapture: () => false,
      onStartShouldSetPanResponder: () => true,
      onPanResponderGrant: () => {
        console.log('inner square grant')
        // this.animateButton()
      },
      onPanResponderRelease: (e, {vx, vy}) => {
        // Animated.timing(this.state.addButtonRightOffset, {
        //   toValue: 20,
        //   duration: 500
        // }).start()
      }
    })
    console.log(this)
  }

  constructor(props) {
    super(props);

    const ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2,
      sectionHeaderHasChanged: (s1, s2) => s1 !== s2
    });
    this.state = {
      dataSource: ds.cloneWithRowsAndSections(this.props.translations)
    }
  }

  getPos(pos) {
    const t = {
      noun: 'Существительное',
      verb: 'Глагол',
      adjective: 'Прилагательное',
      pronoun: 'Местоимение',
      article: 'Артикль',
      averb: 'Наречие',
      number: 'Числительное',
      preposition: 'Предлог',
      conjuction: 'Союз',
      interjection: 'Междометие',
      participle: 'Причастие'
    }
    return t[pos];
  }

  renderSectionHeader(sectionData, category) {
    // console.log(category);
    // console.log(sectionData)
    const tr = sectionData[0].ts;
    const text = sectionData[0].origText;
    return (
      <View style={styles.sectionHeader}>
        <Text style={styles.sectionText}>{this.getPos(category) || category}</Text>
      </View>
    );
  }

  listItem(rowData) {
    // console.log(rowData.toJS());
    const listItemExamplesStyle = (rowData.exs.length > 0) ? styles.listItemExamples : {};
    return (
      <View style={styles.listItem}>
      <ScrollView
        style={styles.listItemTranslations}
        horizontal={true}
        showsHorizontalScrollIndicator={false}>
        <Text style={styles.listItemText}>{rowData.text}</Text>
      </ScrollView>
        <View style={listItemExamplesStyle}>
          {rowData.exs.map((item)=> {
            const str = `${item.text} - ${item.tr}`;
            return (
              <View key={str}>
                <Text style={styles.listItemExamplesText}>{str}</Text>
              </View>
            );
          })}
        </View>
      </View>
    );
  }
  render() {
    let props = this.props || {};
    return (
      <View style={[styles.translationList, props.style]}
      >
        <ListView
          onScroll={this.props.onScroll}
          onScrollAnimationEnd={() => {console.log('end of animation')}}
          removeClippedSubviews={false}
          automaticallyAdjustContentInsets={false}
          dataSource={this.state.dataSource}
          renderRow={this.listItem}
          renderSectionHeader={this.renderSectionHeader.bind(this)}
          />
      </View>
    );
  }
}


const styles = StyleSheet.create({
  translationList: {
    // height: Style.getAdaptHeight(217),
    // marginTop: 15,
    paddingTop: 4,
    alignSelf: 'stretch'
  },
  listItemText: {
    // paddingLeft: 15,
    fontSize: 20,
    lineHeight: 45,
    // marginLeft: 10,
  },
  listItem: {
    backgroundColor: 'white',
    marginTop: 2,
    marginLeft: 10,
    marginRight: 10,
    minHeight: 45,
    shadowOpacity: 1,
    shadowColor: 'rgb(221,221,221)',
    shadowRadius: 2,
    shadowOffset: {
      widht: 0,
      height: 0
    }
  },
  sectionHeader: {
    backgroundColor: '#87cefa',
    height: 25,
    marginLeft: 10,
    marginRight: 10,
    marginTop: 1,
    paddingLeft: 10,
    shadowOpacity: 1,
    shadowColor: 'rgb(131,131,131)',
    shadowRadius: 2,
    shadowOffset: {
      widht: 0,
      height: 0
    }
  },
  sectionText: {
    color: 'white',
    lineHeight: 25,
    fontSize: 20
  },
  listItemExamples: {
    marginLeft: 15,
    marginRight: 15,
    marginBottom: 15
  },
  listItemExamplesText: {
    color: 'rgb(101,101,101)',
    fontSize: 13,
    lineHeight: 18
  },
  listItemTranslations: {
    height: 45,
    flex: 1,
    marginLeft: 15,
    marginRight: 15
  }
});
module.exports = TranslationList;
