import Dimensions from 'Dimensions';

const x = Dimensions.get('window').width;
const y = Dimensions.get('window').height;

const ratioY = (function(){
  if (y <= 480) {
    return 0.35;
  } else if (y <= 568) {
    return 0.75;
  } else if (y <= 667) {
    return 0.915;
  } else {
    return 1;
  }
})();

const ratioX = x < 375 ? (x < 320 ? 0.65 : 0.85) : 1;
// const ratioY = y < 568 ? (x < 480 ? 0.55 : 0.5) : 1;

export default Style = {
  width: x,
  height: y,
  getAdaptHeight(value) {
    return ratioY * value;
  },
  getAdaptWidth(value) {
    return ratioX * value;
  }

}
