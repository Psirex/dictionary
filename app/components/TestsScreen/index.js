import React, {Component} from 'react'
import {
  View,
  Text,
  ListView,
  StyleSheet,
  TouchableOpacity,
  Animated
} from 'react-native'
import { connect } from 'react-redux'
import {bindActionCreators} from 'redux'
import { ActionCreators } from '../../actions'
import { createButtonStyle } from '../../styles'
import Style from '../Style'

const TEST_ITEM_WIDTH_WRAPPER = Style.getAdaptWidth(700)
const TEST_ITEM_WIDTH = Style.getAdaptWidth(322)
const TEST_ITEM_TRANSITION_OFFSET = Style.getAdaptWidth(342)

class TestsScreen extends Component {

  constructor (props) {
    console.log('CONSSSSSSSSSSTRACT')
    super(props)
    this.state = {
      buttonOffset: new Animated.Value(-300),
      showedWord: this.props.first.word,
      selectedItem: -1,
      showAnswer: false,
      showNextButton: false,
      showedExamples: this.props.first.examples,
      itemsOffsets: [new Animated.Value(0), new Animated.Value(0),
        new Animated.Value(0), new Animated.Value(0)],
      secondItemsOpacity: [new Animated.Value(0), new Animated.Value(0),
        new Animated.Value(0), new Animated.Value(0)],
      wordOpacity: new Animated.Value(1)
    }
  }

  onItemPress (index) {
    // this.props.testItemPressed(index)
    this.setState({selectedItem: index})
    setTimeout(() => {
      this.setState({showAnswer: true})
      if (this.props.first.correctIndex !== this.state.selectedItem) {
        this.showNextButton()
      }
    }, 500)
    let timeout = setTimeout(
      () => {
        if (this.props.first.correctIndex !== this.state.selectedItem) {
          clearTimeout(timeout)
          return
        }
        this.nextTestAnimations()
      }, 1500)
  }

  casesToDataSource (cases) {
    return cases.map((item, index) => {
      return {
        title: item,
        index: index
      }
    })
  }

  componentWillReceiveProps (nextProps) {
    this.setState({
      selectedItem: -1,
      showAnswer: false,
      showedWord: nextProps.first.word,
      showedExamples: nextProps.first.examples,
      itemsOffsets: [new Animated.Value(0), new Animated.Value(0),
        new Animated.Value(0), new Animated.Value(0)],
      secondItemsOpacity: [new Animated.Value(0), new Animated.Value(0),
        new Animated.Value(0), new Animated.Value(0)],
      wordOpacity: this.state.wordOpacity
    })
  }

  componentDidUpdate () {
    console.log(this.state.wordOpacity)
    Animated.timing(this.state.wordOpacity, {
      toValue: 1,
      duration: 300
    }).start()
  }

  listItem (firstItem, secondItem) {
    const showAnswer = this.state.showAnswer
    let pressedStyle = (!showAnswer && firstItem.index === this.state.selectedItem) ? styles.pressedItem : {}
    let wrongAnswerStyle = (showAnswer &&
      this.state.selectedItem === firstItem.index &&
      this.state.selectedItem !== this.props.first.correctIndex) ? styles.wrongAnswer : {}
    let correctAnswerStyle = (showAnswer && firstItem.index === this.props.first.correctIndex)
      ? styles.correctAnswer : {}
    return (
      <View style={styles.listItemWrapper}>
        <TouchableOpacity
          activeOpacity={0.7}
          onPress={() => {
            if (!this.state.showAnswer) {
              this.onItemPress(firstItem.index)
            }
          }}>
          <View style={[styles.listItem, pressedStyle, wrongAnswerStyle, correctAnswerStyle]}>
            <Text style={styles.listItemText}>{firstItem.title.toUpperCase()}</Text>
          </View>
        </TouchableOpacity>

        <View style={styles.listItem}>
          <Animated.View style={{opacity: this.state.secondItemsOpacity[secondItem.index]}}>
            <Text style={styles.listItemText}>{secondItem.title.toUpperCase()}</Text>
          </Animated.View>
        </View>
      </View>
    )
  }

  moveButton (offset, callback) {
    Animated.timing(this.state.buttonOffset, {
      toValue: offset,
      duration: 500
    }).start(() => { if (callback) callback() })
  }

  showNextButton () {
    this.moveButton(28)
  }

  hideNextButton () {
    this.moveButton(300, () => {
      this.state.buttonOffset.setValue(-300)
    })
  }

  examplesView (examples) {
    if (examples === undefined) {
      return undefined
    }
    return (
      <View style={styles.examples}>
        {examples.slice(0, 5).map((item) => {
          return (
            <View key={item.text}>
              <Text style={styles.examplesWord}>{item.text}</Text>
            </View>
          )
        })}
      </View>
    )
  }

  nextTestAnimations () {
    let caseItemsAnimations = []
    for (let i = 0; i < 4; ++i) {
      let itemOffset = this.state.itemsOffsets[i]
      let itemOpacity = this.state.secondItemsOpacity[i]
      caseItemsAnimations.push(Animated.parallel(
      [Animated.timing(itemOffset, {
        toValue: TEST_ITEM_TRANSITION_OFFSET,
        duration: 300,
        delay: 100 * i}),
        Animated.timing(itemOpacity, {
          toValue: 1,
          duration: 800,
          delay: 100 * i})
      ]))
    }
    // Animated.parallel(caseItemsAnimations).start()
    let hideWordAnimation = Animated.timing(this.state.wordOpacity, {
      toValue: 0,
      duration: 200
    })
    let showWordAnimation = Animated.timing(this.state.wordOpacity, {
      toValue: 1,
      duration: 200
    })

    hideWordAnimation.start(() => {
      this.setState({
        showedWord: this.props.second.word,
        itemsOffsets: this.state.itemsOffsets,
        secondItemsOpacity: this.state.secondItemsOpacity,
        wordOpacity: this.state.wordOpacity,
        showedExamples: this.props.second.examples
      })
      Animated.parallel([showWordAnimation, ...caseItemsAnimations])
        .start(() => {
          this.props.loadTest('ENG', this.props.second)
        })
    })
  }

  loadButtonView () {
    // let buttonOpacityStyle = this.state.showNextButton ?
    //   {} : {opacity: 0}
    return (
      <Animated.View style={{right: this.state.buttonOffset}}>
        <TouchableOpacity
          onPress={
            () => {
              // this.setState({showNextButton: false})
              this.hideNextButton()
              this.nextTestAnimations()
            }}
          style={styles.nextButton}
        >
          <Text style={styles.nextButtonText}>Далее</Text>
        </TouchableOpacity>
      </Animated.View>
    )
  }

  wordWithExamplesView (word, translations) {
    let examplesView = this.examplesView(translations)
    return (
      <Animated.View style={[styles.wordWithExamplesView, {opacity: this.state.wordOpacity}]}>
        <Text style={styles.word}> {word.toUpperCase()} </Text>
        {examplesView}
      </Animated.View>
    )
  }

  createCases () {
    let firstItem = this.casesToDataSource(this.props.first.cases);
    let secondItem = this.casesToDataSource(this.props.second.cases);
    let res = []
    for (let i = 0; i < 4; ++i) {
      res.push(<Animated.View key={firstItem[i].title + secondItem[i].title}
        style={{right: this.state.itemsOffsets[i]}}>
          {this.listItem(firstItem[i], secondItem[i])}
        </Animated.View>)
    }
    return res
  }

  render () {
    return (
      <View style={styles.wrapper}>
        {this.wordWithExamplesView(this.state.showedWord, this.state.showedExamples)}
        <View style={styles.listViewWrapper}>
          {
            this.createCases()
          }
        </View>
        {this.loadButtonView()}
      </View>
    )
  }
}

function mapDispatchToProps (dispatch) {
  return bindActionCreators(ActionCreators, dispatch)
}

export default connect((state) => {
  return {
    first: {
      word: state.testsScreen.first.word,
      cases: state.testsScreen.first.cases,
      correctIndex: state.testsScreen.first.correctIndex,
      selectedItem: state.testsScreen.first.selectedItem,
      isShowAnswer: state.testsScreen.first.showAnswer,
      examples: state.testsScreen.first.examples
    },
    second: {
      word: state.testsScreen.second.word,
      cases: state.testsScreen.second.cases,
      correctIndex: state.testsScreen.second.correctIndex,
      selectedItem: state.testsScreen.second.selectedItem,
      isShowAnswer: state.testsScreen.second.showAnswer,
      examples: state.testsScreen.second.examples
    }
  }
}, mapDispatchToProps)(TestsScreen)

const styles = StyleSheet.create({
  wordWithExamplesView: {
    marginTop: 20,
    // backgroundColor: 'white',
    minHeight: 170,
    alignSelf: 'stretch',
    flexDirection: 'column',
    alignItems: 'center'
  },
  examplesWord: {
    color: 'rgb(141, 141, 141)'
  },
  listItemWrapper: {
    flexDirection: 'row',
    width: TEST_ITEM_WIDTH_WRAPPER
  },
  wrapper: {
    flex: 1,
    backgroundColor: 'rgb(241, 241, 241)',
    flexDirection: 'column',
    alignItems: 'center',
    alignSelf: 'stretch',
    justifyContent: 'space-between',
    paddingTop: 10,
    paddingLeft: 10,
    paddingRight: 10
  },
  examples: {
    alignSelf: 'stretch',
    alignItems: 'center',
    marginTop: 10
  },
  nextButton: {
    alignSelf: 'center',
    width: 60,
    height: 60,
    backgroundColor: '#f08080',
    position: 'absolute',
    bottom: 60,
    // right: -28,
    borderRadius: 30,
    justifyContent: 'center',
    alignItems: 'center',
    shadowOpacity: 1,
    shadowColor: 'rgba(0, 0, 0, 0.4)',
    shadowRadius: 3,
    shadowOffset: {
      width: 1,
      height: 2
    }
  },
  nextButtonText: {
    fontSize: 16,
    color: 'white'
  },
  word: {
    marginTop: 15,
    fontSize: 30
  },
  listViewWrapper: {
    overflow: 'hidden',
    height: 235,
    marginBottom: 50,
    paddingLeft: Style.getAdaptWidth(7),
    paddingRight: 0,
    flexDirection: 'column',
    alignSelf: 'stretch'
  },
  pressedItem: {
    backgroundColor: '#b0c4de'
  },
  correctAnswer: {
    backgroundColor: '#8fbc8f'
  },
  wrongAnswer: {
    backgroundColor: '#e9967a'
  },
  listItem: {
    height: 55,
    width: TEST_ITEM_WIDTH,
    backgroundColor: 'white',
    marginLeft: Style.getAdaptWidth(10),
    marginRight: Style.getAdaptWidth(10),
    // borderWidth: 1,
    marginTop: 3,
    shadowOpacity: 1,
    shadowColor: 'rgb(221,221,221)',
    shadowRadius: 2,
    shadowOffset: {
      widht: 0,
      height: 0
    }
  },
  listItemText: {
    fontSize: 20,
    lineHeight: 55,
    paddingLeft: 10
  }
})
