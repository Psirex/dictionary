import React, {Component} from 'react'
import { ActionCreators } from '../../actions'
import {
  View,
  Text,
  StyleSheet,
  ListView,
  TouchableOpacity,
  Animated,
  Image
} from 'react-native'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import LanguagePanel from '../LanguagePanel'
import { isEnglishWorld, isRussianWorld } from '../../storage/DictionaryStorage'
import getSectionData from '../../lib/serializers'
import TranslationList from '../TranslationList'
import Style from '../Style'

const TRANSLATION_LIST_HEIGHT = Style.getAdaptHeight(400)
const DURATION = 500

function getSynsString (syns) {
  return syns.reduce((currState, item) => {
    return currState + ', ' + item.text
  }, '')
}

function filterWords (words, lang) {
  let filteredWords = []
  switch (lang) {
    case 'RUS':
      filteredWords = words.filter((word) => {
        return isRussianWorld(word)
      }).sort()
      break
    case 'ENG':
      filteredWords = words.filter((word) => {
        return isEnglishWorld(word)
      }).sort()
      break
  }
  return filteredWords
}

const  TRANSlATE_WIHDOW_HEIGHT = 350

class DictionaryScreen extends Component {
  constructor (props) {
    super(props)
    this.props.loadWords()
    const ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => {
        return r1 !== r2
      },
      sectionHeaderHasChanged: (s1, s2) => {
        return s1 !== s2
      }
    })
    this.state = {
      dataSource: this.initDataSource(this.props.from,
        this.props.words, ds),
      marginTop: new Animated.Value(-TRANSlATE_WIHDOW_HEIGHT),
      marginBottom: new Animated.Value(50),
      translationListBottomOffset: new Animated.Value(-TRANSLATION_LIST_HEIGHT)
    }
  }

  initDataSource (from, words, ds) {
    let res = words.reduce((currentState, word) => {
      if (currentState[word.charCodeAt(0)] === undefined) {
        currentState[word.charCodeAt(0)] = []
      }
      currentState[word.charCodeAt(0)].push(word)
      return currentState
    }, {})
    console.log(res)
    return ds.cloneWithRowsAndSections(res)
  }

  componentWillReceiveProps (nextProps) {
    this.setState({
      dataSource: this.initDataSource(nextProps.from,
        nextProps.words, this.state.dataSource),
      marginTop: this.state.marginTop
    })
  }

  emptyView () {
    return (
      <View style={styles.emptyView}>
        <Text style={styles.emptyViewText}>
          Здесь появятся слова,{'\n'}
          которые вы добавили
        </Text>
      </View>
    )
  }

  listViewItem (word, onPressItem) {
    return (
      <TouchableOpacity
        onPress={onPressItem}>
        <View style={styles.listItem}>
          <Text style={styles.listItemText}>{word}</Text>
        </View>
      </TouchableOpacity>
    )
  }

  showTranslations () {
    Animated.timing(
        this.state.marginTop, {toValue: 0}
    ).start()
  }

  hideTranslations () {
    Animated.timing(
        this.state.marginTop, {toValue: -TRANSlATE_WIHDOW_HEIGHT}
    ).start()
  }

  fullfilledView () {
    return (
      <ListView
        dataSource={this.state.dataSource}
        renderRow={
          (rowData) => {
            return this.listViewItem(rowData, () => {
              this.props.loadWord(rowData)
              this.showTranslations()
              console.log('wheeeeeha')
              Animated.parallel([
                Animated.timing(this.state.translationListBottomOffset, {
                  toValue: 0,
                  duration: DURATION
                }),
                Animated.timing(this.state.marginBottom, {
                  toValue: TRANSLATION_LIST_HEIGHT,
                  duration: DURATION
                })
              ]).start()
            })
          }
        }
        renderSectionHeader={(sectionData, sectionId) => {
          return <Text style={{paddingLeft: 10}}>{String.fromCharCode(sectionId)}</Text>
        }}
      />
    )
  }

  onCloseButtonClick () {
    Animated.parallel([
      Animated.timing(
        this.state.translationListBottomOffset, {
          toValue: -TRANSLATION_LIST_HEIGHT,
          duration: DURATION
        }
      ),
      Animated.timing(this.state.marginBottom, {
        toValue: 50,
        duration: DURATION
      })
    ]).start()
  }

  translationListView (word, translations) {
    return (
      <View>
        <View style={styles.title}>
          <View style={styles.wordWrapper}>
            <Text style={styles.word}>{word}</Text>
          </View>
          <TouchableOpacity
            style={styles.closeButton}
            onPress={() => this.onCloseButtonClick()}
            >
            <Image
              style={styles.closeButtomIcon}
              source={require('./img/close.png')}
              resizeMode={'cover'} />
          </TouchableOpacity>
        </View>
        <View style={styles.translationsListWrapper}>
          <TranslationList
            style={{height: 250}}
            translations={translations} />
        </View>
      </View>
    )
  }

  render () {
    let renderedView
    if (this.props.words.length === 0) {
      renderedView = this.emptyView()
    } else {
      renderedView = this.fullfilledView()
    }
    //!this.props.showedItem.isEmpty()
    let translationList = (true)
      ? this.translationListView(this.props.showedItem.get('word'),
      getSectionData(this.props.showedItem.get('translations').toJS())) : undefined
    return (
      <View style={styles.wrapper}>
        <View style={styles.panel}>
          <LanguagePanel
            from={this.props.from}
            to={this.props.to}
            swapLanguages={() => {
              this.props.swapDictionaryLanguages()
            }} />
        </View>
        <Animated.View style={[styles.listViewWrapper, {marginBottom: this.state.marginBottom}]}>
          {renderedView}
        </Animated.View>
        <Animated.View style={[styles.translation, {bottom: this.state.translationListBottomOffset}]}>
          {translationList}
        </Animated.View>
      </View>
    )
  }
}

function mapDispatchToProps (dispatch) {
  return bindActionCreators(ActionCreators, dispatch)
}

export default connect((state) => {
  return {
    words: filterWords(state.dictionaryScreen.get('words').toJS(), state.dictionaryScreen.get('from')),
    from: state.dictionaryScreen.get('from'),
    to: state.dictionaryScreen.get('to'),
    showedItem: state.dictionaryScreen.get('showedItem')
  }
}, mapDispatchToProps)(DictionaryScreen)

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    backgroundColor: 'rgb(241, 241, 241)',
    flexDirection: 'column',
    alignItems: 'center',
    alignSelf: 'stretch',
    justifyContent: 'center',
    paddingTop: 20
  },
  translation: {
    borderTopWidth: 1,
    borderColor: 'rgb(181, 181, 181)',
    backgroundColor: 'rgba(241, 241, 241, 0.1)',
    position: 'absolute',
    zIndex: 2,
    bottom: -TRANSLATION_LIST_HEIGHT,
    right: 0,
    left: 0,
    height: TRANSLATION_LIST_HEIGHT
  },
  listViewWrapper: {
    flex: 1,
    flexDirection: 'row'
  },
  panel: {
    marginTop: 15
  },
  listItemText: {
    fontSize: 15,
    paddingLeft: 10,
    lineHeight: 40
  },
  emptyView: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  emptyViewText: {
    fontSize: 25,
    color: 'rgb(201, 201, 201)'
  },
  listItem: {
    backgroundColor: 'white',
    marginLeft: 10,
    marginRight: 10,
    height: 40,
    marginTop: 3,
    shadowOpacity: 1,
    shadowColor: 'rgb(221,221,221)',
    shadowRadius: 2,
    shadowOffset: {
      widht: 0,
      height: 0
    }
  },
  word: {
    textAlign: 'center',
    fontSize: 22
  },
  wordWrapper: {
    flex: 1,
    alignItems: 'center'
  },
  title: {
    marginLeft: 20,
    marginTop: 5,
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  closeButton: {
    width: 25,
    height: 25,
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 5
  },
  closeButtomIcon: {
    height: 25,
    width: 25
  },
})
