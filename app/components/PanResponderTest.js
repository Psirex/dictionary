import React, { Component } from 'react'
import {
  View,
  Text,
  PanResponder,
  Animated,
  ListView,
  ScrollView
} from 'react-native'

class PanResponderTest extends Component {

  constructor (props) {
    super(props)
    const ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    })
    this.state = {
      dataSource: ds.cloneWithRows(['row 1', 'row 2'])
    }
  }

  componentWillMount () {
    this._panResponder = PanResponder.create({
      onMoveShouldSetPanResponderCapture: () => false,
      onStartShouldSetPanResponderCapture: () => false,
      onStartShouldSetPanResponder: () => true,
      onMoveShouldSetResponder: () => true,
      onPanResponderRelease: (e, {vx, vy}) => {
        console.log('heyyyyy')
      },
      onPanResponderGrant: (e, {vx, vy}) => {
        console.log('Grant')
      }
    })
    this._panResponderInner = PanResponder.create({
      onMoveShouldSetPanResponderCapture: () => true,
      onStartShouldSetPanResponderCapture: () => true,
      onStartShouldSetPanResponder: () => true,
      onPanResponderGrant: () => {
        console.log('inner square grant')
      },
      onPanResponderReject: () => {
        console.log('rejected')
      }
    })
  }

  render () {
    let { pan } = this.state
    return (
      <View style={styles.container}>
        <View style={styles.circle}
        {...this._panResponder.panHandlers}
        >
          <View style={styles.innerSquare}
          {...this._panResponderInner.panHandlers}>
          </View>
          <ScrollView
          {...this._panResponderInner.panHandlers}>
            <Text>text 1</Text>
            <Text>text 1</Text>
            <Text>text 1</Text>
            <Text>text 1</Text>
          </ScrollView>
        </View>
      </View>
    )
  }
}

module.exports = PanResponderTest
const styles = {
  container: {
    flex: 1,
    backgroundColor: 'green',
    alignItems: 'center',
    justifyContent: 'center'
  },
  circle: {
    width: 150,
    height: 150,
    backgroundColor: 'red',
    borderRadius: 75,
    justifyContent: 'center',
    alignItems: 'center'
  },
  innerSquare: {
    width: 40,
    height: 40,
    backgroundColor: 'blue'
  }
}
