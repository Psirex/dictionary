import React, {Component} from 'react'
import Style from './Style'
import {
  View,
  TextInput,
  StyleSheet,
  Animated,
  Image,
  TouchableOpacity
} from 'react-native'

const LanguagePanel = require('./LanguagePanel')
const INITIAL_HEIGHT = 90
class TranslationPanel extends Component {
  constructor (props) {
    super(props)
    this.state = {
      textHeight: INITIAL_HEIGHT,
      textInputHeight: new Animated.Value(INITIAL_HEIGHT)
    }
  }

  onFocus () {
    Animated.timing(
      this.state.textInputHeight,
      {toValue: 200}
    ).start()
  }

  onChangeText (event) {
    console.log(event.nativeEvent.contentSize.height)
    // this.props.changeWord()
  }

  onChange (event) {
    const textInputHeight = event.nativeEvent.contentSize.height
    const newValue = Math.min(textInputHeight, 180)
    this.animateHeight(newValue + 40)
  }

  animateHeight (newValue) {
    this.setState({
      textHeight: newValue,
      textInputHeight: this.state.textInputHeight
    })
    Animated.timing(
     this.state.textInputHeight,
      {
        toValue: newValue,
        duration: 150
      }).start()
  }

  onSubmit (event) {
    this.props.onSubmit(this.state.textHeight)
    this.props.translate(this.props.word,
    this.props.from.slice(0, 2), this.props.to.slice(0, 2))
    console.log(event.nativeEvent)
  }
  render () {
    let clearButtonOpacity = this.props.word ? {opacity: 1} : {opacity: 0};
    return (
      <Animated.View style={[styles.translationPanel, {height: this.state.textInputHeight}]}>
        <TextInput style={styles.textInput}
          blurOnSubmit={true}
          multiline={true}
          onChangeText={this.props.changeWord}
          onChange={this.onChange.bind(this)}
          onEndEditing={this.onSubmit.bind(this)}
          value={this.props.word}
          placeholder={'Введите текст здесь...'}
          keyboardType={'default'}/>
        <View style={styles.languagePanel}>
          <View style = {clearButtonOpacity}>
            <TouchableOpacity
              onPress={
                () => {
                  this.props.changeWord('')
                  this.animateHeight(INITIAL_HEIGHT)
                }
              }>
                <Image style={styles.clearButton} source={require('./img/clear.png')}/>
            </TouchableOpacity>
          </View>
          <LanguagePanel
            from={this.props.from}
            to={this.props.to}
            swapLanguages={() => {
              // console.log('fdfdfdfdf')
              this.props.swapLanguagesAndTranslate(this.props.word, this.props.from.slice(0, 2), this.props.to.slice(0, 2))
            }}/>
        </View>
      </Animated.View>
    )
  }
}

const LANGUAGE_PANEL_HEIGHT = 38
const styles = StyleSheet.create({
  translationPanel: {
    // height: Style.getAdaptHeight(HEIGHT),
    justifyContent: 'flex-end',
    marginLeft: 10,
    marginRight: 10,
    backgroundColor: 'white',
    shadowColor: 'rgb(221,221,221)',
    shadowRadius: 3,
    shadowOffset: {
      widht: 0,
      height: 0
    },
    shadowOpacity: 1
  },
  textInput: {
    padding: 10,
    // minHeight: 40,
    // height: 150,
    flex: 1,
    // backgroundColor: 'green',
    // height: Style.getAdaptHeight(TEXT_INPUT_HEIGHT),
    fontSize: 20
  },
  languagePanel: {
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: Style.getAdaptHeight(4),
    height: Style.getAdaptHeight(LANGUAGE_PANEL_HEIGHT),
    justifyContent: 'space-between',
    flexDirection: 'row',

  },
  clearButton: {
    // backgroundColor: 'blue',
    width: 25,
    height: 25,
    alignItems: 'center',
    justifyContent: 'center',
  },
  clearButtonText: {
    textAlign: 'center',
    fontSize: 20,
    lineHeight: 18
  }
})

module.exports = TranslationPanel
