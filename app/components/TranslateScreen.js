import React, {Component} from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { ActionCreators } from '../actions'
import DictionaryStorage from '../storage/DictionaryStorage'
import {
  View,
  StyleSheet,
  Text,
  Animated,
  TouchableOpacity,
  PanResponder
} from 'react-native'

import PanResponderTest from './PanResponderTest'

const TranslationPanel = require('./TranslationPanel')
import TranslationResultList from './TranslationResultList'
import Style from './Style'
import { createButtonStyle } from '../styles'

const RESULT_LIST_HEIGHT = 600

class TranslateScreen extends Component {
  constructor (props) {
    super(props)
    console.log(Style.height)
    console.log(this.props)
    this.state = {
      bounceValue: new Animated.Value(0),
      resultListHeight: 80
    }
    // console.log(createButton(150, 40, {marginTop: 25, alignSelf: 'center'}));
  }

  setResultListHeight (newValue) {
    console.log(newValue)
    this.setState({
      bounceValue: this.state.bounceValue,
      resultListHeight: newValue
    })
  }

  componentWillReceiveProps (nextProps) {
      console.log(nextProps)
  }

  componentWillMount () {
    this._panResponder = PanResponder.create({
      // onStartShouldSetPanResponder: (evt, gestureState) => true,
      // onStartShouldSetPanResponderCapture: (evt, gestureState) => true,
      onMoveShouldSetResponderCapture: () => true,
      onMoveShouldSetPanResponderCapture: () => true,
      onPanResponderGrant: (evt, gestureState) => {
        console.log('onPanResponderGrant')
      },
      onPanResponderMove: (evt, gestureState) => {
        console.log('onPanResponderMove')
      },
      onPanResponderRelease: (evt, gestureState) => {
        console.log('onPanResponderMove')
      }

    })
    console.log(this)
  }

  componentDidMount () {
    Animated.timing(this.state.bounceValue, {
      toValue: 1
    }).start()
  }

  isDissabled () {
    return this.props.wordWasTranslated === '' ||
      this.props.translations.length === 0
  }

  buttonView () {
    let dissabledStyle = this.isDissabled() ? styles.dissabledButton : {}
    return (
      <TouchableOpacity
        disabled={this.isDissabled()}
        onPress={() => {
          this.props.saveWord(this.props.wordWasTranslated, this.props.translations)
        }}
        style={styles.button}
      >
        <Text style={[styles.buttonText, dissabledStyle]}>Добавить в словарь</Text>
      </TouchableOpacity>
    )
  }

  render () {
    // return (
    //   <PanResponderTest />
    // );
    return (
      <Animated.View style={[styles.mainWindow, {opacity: this.state.bounceValue}]}>
        <TranslationPanel
          changeWord={this.props.changeWord}
          word={this.props.word}
          translate={this.props.translate}
          from={this.props.from}
          to={this.props.to}
          onSubmit={this.setResultListHeight.bind(this)}
          swapLanguagesAndTranslate={this.props.swapLanguagesAndTranslate}
        />
        <TranslationResultList
          onAddButtonPressed={() => {
            this.props.saveWord(this.props.wordWasTranslated, this.props.translations)
          }}
          isPresented={this.props.isPresented}
          translations={this.props.translations}
          isLoading={this.props.isLoading}
          wordWasTranslated={this.props.wordWasTranslated}
          style={{height: Style.getAdaptHeight(RESULT_LIST_HEIGHT) - this.state.resultListHeight}}
        />
      </Animated.View>
    )
  }
}

var styles = StyleSheet.create({
  mainWindow: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'stretch',
    alignSelf: 'stretch',
    justifyContent: 'flex-start',
    paddingTop: 40,
    // paddingLeft: 10,
    // paddingRight: 10,
    opacity: 0,
    backgroundColor: 'rgb(241, 241, 241)'
  },
  title: {
    fontSize: 30,
    marginTop: Style.getAdaptHeight(10),
    marginBottom: Style.getAdaptHeight(20)
  },
  button: createButtonStyle(160, 40, { alignSelf: 'center', marginBottom: 20 }),
  dissabledButton: {
    color: 'rgb(201, 201, 201)'
  },
  buttonText: {
    fontSize: 15,
    textAlign: 'center',
    borderRadius: 5
  }
})

function mapDispatchToProps (dispatch) {
  return bindActionCreators(ActionCreators, dispatch)
}

export default connect((state) => {
  console.log(state)
  return {
    word: state.translationScreen.get('word'),
    translations: state.translationScreen.get('translations').toJS(),
    from: state.translationScreen.get('from'),
    to: state.translationScreen.get('to'),
    isLoading: state.translationScreen.get('isLoading'),
    wordWasTranslated: state.translationScreen.get('wordWasTranslated'),
    isPresented: state.translationScreen.get('isPresented')
  }
}, mapDispatchToProps)(TranslateScreen)
