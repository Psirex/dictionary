import React, {Component} from 'react'
import Style from './Style'
import {
  View,
  StyleSheet,
  Text,
  ActivityIndicator,
  PanResponder,
  Animated,
  TouchableOpacity
} from 'react-native'

import TranslationList from './TranslationList'
import getSectionData from '../lib/serializers'

const INITIAL_BUTTON_OFFSET = -100
const INITIAL_TOP_OFFSET = 600

export default class TranslationResultList extends Component {
  constructor (props) {
    super(props)
    this.state = {
      addButtonRightOffset: new Animated.Value(INITIAL_BUTTON_OFFSET),
      topOffset: new Animated.Value(INITIAL_TOP_OFFSET)
    }
  }

  componentDidMount () {
    this.state.topOffset.setValue(INITIAL_TOP_OFFSET)
    this.state.addButtonRightOffset.setValue(INITIAL_BUTTON_OFFSET)
  }
  componentWillUpdate (nextProps, nextState) {
    console.log('next props is')
    console.log(nextProps)
    if (nextProps.isLoading) {
      nextState.topOffset.setValue(INITIAL_TOP_OFFSET)
      nextState.addButtonRightOffset.setValue(INITIAL_BUTTON_OFFSET)
    }
  }

  showTranslations () {
    console.log('result list was mounted')
    Animated.sequence([
      Animated.timing(this.state.topOffset, {
        toValue: 0,
        duration: 500
      }),
      this.buttonAnimation(20)]).start()
  }

  buttonAnimation (value) {
    return Animated.timing(this.state.addButtonRightOffset, {
      toValue: value,
      duration: 500
    })
  }

  getViewState () {
    console.log(this.props)
    if (this.props.isLoading) {
      return 'LOADING'
    } else if (this.props.translations.length !== 0) {
      return 'FINDED'
    } else if (this.props.translations.length === 0 && this.props.wordWasTranslated !== '') {
      return 'NOT_FINDED'
    }
    return 'EMPTY_VIEW'
  }

  emptyView () {
    return (
      <View style={styles.loadingView}>
        <Text style={styles.emptyListText}>Перевод будет здесь...</Text>
      </View>
    )
  }

  loadingView () {
    return (
      <View style={styles.loadingView}>
        <ActivityIndicator
          animating={this.props.isLoading}
          size='large' />
      </View>
    )
  }

  addButtonView () {
    console.log(this.props.isPresented)
    return !this.props.isPresented ?
      <Animated.View style={[styles.addButton, {right: this.state.addButtonRightOffset}]}>
        <TouchableOpacity
          onPress={() => {
            this.buttonAnimation(-100).start(() => {
              this.props.onAddButtonPressed()
            })
          }}>
          <Text style={styles.addButtonText}>+</Text>
        </TouchableOpacity>
      </Animated.View>
    : undefined
  }

  findedView () {
    console.log(getSectionData(this.props.translations))
    return (
      <Animated.View style={[styles.findedView, {top: this.state.topOffset}]}>
        <TranslationList
          translations={getSectionData(this.props.translations)}
          style={this.props.style}
          panResponder={this._panResponderInner}
          onDidMount={this.showTranslations.bind(this)}
          />
        {this.addButtonView()}
      </Animated.View>
    )
  }

  notFindedView () {
    return (
      <View style={styles.loadingView}>
        <Text style={styles.emptyListText}>Упс. Ничего не нашли...</Text>
      </View>
    )
  }

  render () {
    const state = this.getViewState()
    console.log(state)
    let renderedView
    switch (state) {
      case 'LOADING':
        renderedView = this.loadingView()
        break
      case 'FINDED':
        renderedView = this.findedView()
        break
      case 'NOT_FINDED':
        renderedView = this.notFindedView()
        break
      default:
        renderedView = this.emptyView()
    }
    return (
      <View style={[styles.translationResultList, this.props.style]}
      >
        {renderedView}
      </View>
    )
  }
}

const HEIGHT = 160

const styles = StyleSheet.create({
  translationResultList: {
    marginTop: 15,
    height: Style.getAdaptHeight(HEIGHT)
  },
  addButton: {
    width: 50,
    height: 50,
    backgroundColor: '#f08080',
    position: 'absolute',
    bottom: 10,
    right: -100,
    borderRadius: 25,
    justifyContent: 'center',
    alignItems: 'center',
    shadowOpacity: 1,
    shadowColor: 'rgba(0, 0, 0, 0.4)',
    shadowRadius: 3,
    shadowOffset: {
      width: 1,
      height: 2
    }
  },
  addButtonText: {
    color: 'white',
    fontSize: 30,
    lineHeight: 30
  },
  emptyListWrapper: {
    paddingLeft: 10,
    backgroundColor: 'white',
    height: 50
  },
  emptyListText: {
    color: 'rgb(201, 201, 201)',
    lineHeight: 50,
    fontSize: 20
  },
  loadingView: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1
  },
  findedView: {
    position: 'absolute',
    right: 0,
    left: 0
  }
})
